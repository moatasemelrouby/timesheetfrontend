import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { taskService } from './services/task.service';
import { FormsModule } from '@angular/forms';
import { timeSheetComponent } from './timeSheet/timeSheet.component';
@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    timeSheetComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    EmployeeService,
    taskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
