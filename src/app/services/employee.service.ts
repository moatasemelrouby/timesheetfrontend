import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';

@Injectable()
export class EmployeeService {
  private baseapi = environment.apiUrl;
  employees: Employee[] = new Array<Employee>();
    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get(this.baseapi + "/employee/getall");
  }
  
}

