import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import { Employee } from '../models/employee';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
  
  constructor(private employeeService: EmployeeService, public router: Router, private _Route: ActivatedRoute) {
    
  }

    ngOnInit() {
       
  }

  goToTimesheet() {
    this.router.navigate(['/timeSheet']);
  }
}
