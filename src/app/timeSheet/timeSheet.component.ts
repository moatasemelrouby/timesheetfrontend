import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { taskService } from '../services/task.service';
import { weeklyTimesheet } from '../models/weeklyTimesheet';
import { Employee } from '../models/employee';

@Component({
  selector: 'time-Sheet',
  templateUrl: 'timeSheet.component.html',
  styleUrls: ['./timeSheet.component.scss']
})

export class timeSheetComponent implements OnInit {
  empID: any;
  tasks: any;
  weeklyTimesheetObj: weeklyTimesheet[];
  selectedEmployeeData: Employee;
  constructor(private taskService: taskService, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.selectedEmployeeData = new Employee();
    this.selectedEmployeeData.weekEffort = new Array<weeklyTimesheet>();

    this.selectedEmployeeData.totalSaturday = 0;
    this.selectedEmployeeData.totalSunday = 0;
    this.selectedEmployeeData.totalMonday = 0;
    this.selectedEmployeeData.totalTuesday = 0;
    this.selectedEmployeeData.totalWednesday = 0
    this.selectedEmployeeData.totalThursday = 0
    this.selectedEmployeeData.totalFriday

    this.taskService.getallTasks().subscribe(data => {
      this.tasks = data;
      console.log(this.tasks);
    });


  }
  selectedEmp(empID): void {
    debugger;

    this.selectedEmployeeData = this.employeeService.employees.find(i => i.id == empID);
  }
  addNewItem(): void {
    if (this.empID) {


      if (this.selectedEmployeeData.weekEffort == null || this.selectedEmployeeData.weekEffort.length == 0) {
        this.selectedEmployeeData.weekEffort = new Array<weeklyTimesheet>();

      }
      this.selectedEmployeeData.weekEffort.push(new weeklyTimesheet());
    }
  }

  save(): void {
    this.employeeService.employees.find(i => i.id == this.empID).weekEffort = this.selectedEmployeeData.weekEffort;
    this.employeeService.employees.find(i => i.id == this.empID).totalSaturday = this.selectedEmployeeData.totalSaturday;
    this.employeeService.employees.find(i => i.id == this.empID).totalSunday = this.selectedEmployeeData.totalSunday;
    this.employeeService.employees.find(i => i.id == this.empID).totalMonday = this.selectedEmployeeData.totalMonday;
    this.employeeService.employees.find(i => i.id == this.empID).totalTuesday = this.selectedEmployeeData.totalTuesday;
    this.employeeService.employees.find(i => i.id == this.empID).totalWednesday = this.selectedEmployeeData.totalWednesday;
    this.employeeService.employees.find(i => i.id == this.empID).totalThursday = this.selectedEmployeeData.totalThursday;
    this.employeeService.employees.find(i => i.id == this.empID).totalFriday = this.selectedEmployeeData.totalFriday;
    this.employeeService.employees.find(i => i.id == this.empID).totalWeeklyEffort = this.selectedEmployeeData.totalWeeklyEffort;
    this.employeeService.employees.find(i => i.id == this.empID).AvgWeeklyEffort = this.selectedEmployeeData.AvgWeeklyEffort;

  }
  calcUateTotalsunday(item): void {
    debugger;
    this.selectedEmployeeData.totalSunday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalSunday += this.selectedEmployeeData.weekEffort[i].sunday;
    }
    this.calculateAllTotal();

  }
  calcUateTotalmonday(item): void {
    this.selectedEmployeeData.totalMonday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalMonday += this.selectedEmployeeData.weekEffort[i].monday;
    }
    this.calculateAllTotal();
  }
  calcUateTotaltuesday(item): void {
    this.selectedEmployeeData.totalTuesday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalTuesday += this.selectedEmployeeData.weekEffort[i].tuesday;
    }
    this.calculateAllTotal();
  }
  calcUateTotalwednesday(item): void {
    this.selectedEmployeeData.totalWednesday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalWednesday += this.selectedEmployeeData.weekEffort[i].wednesday;
    }
    this.calculateAllTotal();
  }
  calcUateTotalthursday(item): void {
    this.selectedEmployeeData.totalThursday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalThursday += this.selectedEmployeeData.weekEffort[i].thursday;
    }
    this.calculateAllTotal();
  }
  calcUateTotalfriday(item): void {
    this.selectedEmployeeData.totalFriday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalFriday += this.selectedEmployeeData.weekEffort[i].friday;
    }
    this.calculateAllTotal();
  }
  calcUateTotalsaturday(item): void {
    this.selectedEmployeeData.totalSaturday = 0;
    for (var i = 0; i < this.selectedEmployeeData.weekEffort.length; i++) {

      this.selectedEmployeeData.totalSaturday += this.selectedEmployeeData.weekEffort[i].saturday;
    }
    this.calculateAllTotal();
  }
  calculateAllTotal() {

    this.selectedEmployeeData.totalWeeklyEffort =
      this.selectedEmployeeData.totalSaturday + this.selectedEmployeeData.totalSunday
      + this.selectedEmployeeData.totalMonday + this.selectedEmployeeData.totalTuesday
      + this.selectedEmployeeData.totalWednesday + this.selectedEmployeeData.totalThursday
      + this.selectedEmployeeData.totalFriday;
    this.selectedEmployeeData.AvgWeeklyEffort = this.selectedEmployeeData.totalWeeklyEffort / 7;
  }





}
