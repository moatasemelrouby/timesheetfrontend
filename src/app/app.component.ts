import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './services/employee.service';
import { Employee } from './models/employee';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  emps: any;
  constructor(private employeeService: EmployeeService) {

  }
  ngOnInit() {
    this.employeeService.getallemployees().subscribe(data => {
      this.emps = data;
      this.employeeService.employees = this.emps;
    });
  }
   
}
