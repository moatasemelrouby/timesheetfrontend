import { Injectable } from '@angular/core';
import { weeklyTimesheet } from './weeklyTimesheet';
 
@Injectable()
export class Employee {
  id: any;
  code: any;
  name: any;

  weekEffort: weeklyTimesheet[] = new Array<weeklyTimesheet>();
  totalSunday: any=0;
  totalMonday: any = 0;
  totalTuesday: any = 0;
  totalWednesday: any = 0;
  totalThursday: any = 0;
  totalFriday: any = 0;
  totalSaturday: any = 0;
  totalWeeklyEffort: any = 0;
  AvgWeeklyEffort: any = 0;
}

