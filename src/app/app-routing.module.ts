 
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { EmployeeListComponent } from './employee/employee.component';
 
import { timeSheetComponent } from './timeSheet/timeSheet.component';
 
const routes: Routes = [
  
  {
    path: 'timesheet',
    component: timeSheetComponent 
  }, 
  {
    path: '',
    component: EmployeeListComponent 
  } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
